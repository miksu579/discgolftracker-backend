//IDEOITA:
// FRONT:ID:tä ei tarvii generoida ite scoreen eikä courseen!!

//joskus ehkä
// USER-taulu
// user_id : generoituu ite
// username : varchar
// password : password

// COURSE-taulu
// id : generoituu ite
// nimi : varchar
// viite tauluun missä jokaisen reiän par

// HOLE-taulu
// hole_id: generoituu ite
// rata_id: sama ku sen radalla
// holepar : int
// hole_number : int

// kun haluu radan väylien määrän, ni haetaan sqllä ja lasketaan rivien määrä.

// ROUND-taulu
// round_id : generoituu ite

// HOLE_SCORE-taulu
// hole_score_id : generoituu ite
// round_id : round_id
// rata_id : rata_id
// hole_id : hole_id
// user_id : user_id ??
// timestamp : DateAndTime
// score : int

// valitaan rata -> kierros alkaa -> mennään väylälle -> väylältä tulos -> kun kaikki väylät heitetty ->
// koostetaan kaikista väylistä yhteenveto (väylät localStoragessa), tallennetaan uusi roundi ja väylät DBhen

const express = require("express");
const app = express();
const fs = require("fs");

const bodyParser = require("body-parser");
// const mongoose = require("mongoose");
const port = 5000;

app.use(bodyParser.json());
//CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Auhtorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

app.get("/courseData", (req, res) => {
  fs.readFile("db/courses.json", "utf8", (err, data) => {
    if (err) {
      res.sendStatus(500);
      console.log(err);
      return;
    }
    let courseData = JSON.parse(data);
    res.send(courseData.courseData);
  });
});
app.get("/scoreData", (req, res) => {
  fs.readFile("db/scores.json", "utf8", (err, data) => {
    if (err) {
      res.sendStatus(500);
      console.log(err);
      return;
    }
    let scoreData = JSON.parse(data);
    res.send(scoreData.scoreData);
  });
});

app.post("/saveScore", (req, res) => {
  let scoreData = [];
  fs.readFile("db/scores.json", "utf8", (err, data) => {
    if (err) {
      res.sendStatus(500);
      console.log(err);
      return;
    }
    scoreData = JSON.parse(data);
    let newScore = req.body.newScore;
    scoreData.scoreData.push(newScore);
    let dataToWrite = JSON.stringify(scoreData, null, 2);

    fs.writeFile("db/scores.json", dataToWrite, err => {
      if (err) {
        res.sendStatus(500);
        console.log(err);
        return;
      }
      console.log("Score saved");
      res.sendStatus(200);
    });
  });
});

app.post("/savecourse", (req, res) => {
  let courseData = [];
  fs.readFile("db/courses.json", "utf8", (err, data) => {
    if (err) {
      res.sendStatus(500);
      console.log(err);
      return;
    }
    courseData = JSON.parse(data);
    let newcourse = req.body.newCourse;
    courseData.courseData.push(newcourse);
    let dataToWrite = JSON.stringify(courseData, null, 2);

    fs.writeFile("db/courses.json", dataToWrite, err => {
      if (err) {
        res.sendStatus(500);
        console.log(err);
        return;
      }
      console.log("course saved");
      res.sendStatus(200);
    });
  });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
